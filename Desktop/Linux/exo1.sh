#!/bin/bash
note=-1

until [ $note -le 20 ] && [ $note -ge 0 ]
do
read -p "Veuillez saisir une note entre 0 et 20 : " note
done
#si la note est 20 c'est parfait
if [ $note == 20 ]; then
   echo "$note = Parfait"
   #si la note est supérieure ou égale à 18 c'est excellent
elif [ $note -ge 18 ];
then
   echo "$note = Excellent"
   #si la note est supérieure ou égale à 16 c'est très bien
 elif [ $note -ge 16 ];
 then
    echo "$note = Très bien"
    #si la note est supérieure ou égale à 14 c'est bien
  elif [ $note -ge 14 ];
  then
     echo "$note = Bien"
     #si la note est supérieure ou égale à 12 c'est assez bien
   elif [ $note -ge 12 ];
   then
      echo "$note = Assez bien"
      #si la note est inférieur à 10 c'est Insuffisant
    elif [ $note -le 10 ];
    then
       echo " $note =Insuffisant"

fi
